angular.module(
  'tallytime',
  ['ionic', 'tallytime.controllers', 'tallytime.services', 'chart.js']
)

.run( function( DatabaseService ) {
  DatabaseService.init();
})

.run( function( $ionicPlatform ) {
  $ionicPlatform.ready( function() {
    // Hide the accessory bar by default (remove this to show
    // the accessory bar above the keyboard for form inputs)
    if( window.cordova &&
        window.cordova.plugins &&
        window.cordova.plugins.Keyboard ) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar( true );
      cordova.plugins.Keyboard.disableScroll( true );
    }

    if( window.StatusBar ) {
      StatusBar.styleLightContent();
    }

    // Set up the chart to show percentage value tooltips.
    Chart.defaults.global.tooltipTemplate =
      "<%=label%>: <%= Math.round(circumference / 6.283 * 100) %>%"
    ;

    // Allows coding a formatted string similar to printf.
    // See: http://stackoverflow.com/a/18234317
    if( !String.prototype.format ) {
      String.prototype.format = function() {
        var str = this.toString();

        if( !arguments.length ) {
          return str;
        }

        var args = typeof arguments[0],
            args = (("string" == args || "number" == args) ? arguments : arguments[0]);

        for( arg in args ) {
          str = str.replace( RegExp( "\\{" + arg + "\\}", "gi" ), args[arg] );
        }

        return str;
      }
    }
  });
})

.config( function( $stateProvider, $urlRouterProvider ) {
  $stateProvider

  // setup an abstract state for the tabs directive
  .state( 'tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:
  .state( 'tab.timers', {
    url: '/timers',
    views: {
      'tab-timers': {
        templateUrl: 'templates/tab-timers.html',
        controller: 'LabelsCtrl'
      }
    }
  })

  .state( 'tab.edits', {
    url: '/edits',
    views: {
      'tab-edits': {
        templateUrl: 'templates/tab-edits.html',
        controller: 'EditsCtrl'
      }
    }
  })

  .state( 'tab.labels', {
    url: '/labels',
    views: {
      'tab-labels': {
        templateUrl: 'templates/tab-labels.html',
        controller: 'LabelsCtrl'
      }
    }
  })

  .state( 'tab.reports', {
    url: '/reports',
    views: {
      'tab-reports': {
        templateUrl: 'templates/tab-reports.html',
        controller: 'ReportsCtrl'
      }
    }
  })

  .state( 'tab.reports-time', {
    url: '/reports/time',
    views: {
      'tab-reports': {
        templateUrl: 'templates/tab-reports-time.html',
        controller: 'ReportsTimeCtrl',
        resolve: {
          eventLabels: function( ReportFactory ) {
            return ReportFactory.queryEventLabels();
          },
          times: function( ReportFactory ) {
            return ReportFactory.queryTimes();
          }
        }
      }
    }
  })

  .state( 'tab.reports-tally', {
    url: '/reports/tally',
    views: {
      'tab-reports': {
        templateUrl: 'templates/tab-reports-tally.html',
        controller: 'ReportsTallyCtrl',
        resolve: {
          eventLabels: function( ReportFactory ) {
            return ReportFactory.queryEventLabels();
          },
          tallies: function( ReportFactory ) {
            return ReportFactory.queryTallies();
          }
        }
      }
    }
  })

  .state( 'tab.settings', {
    url: '/settings',
    views: {
      'tab-settings': {
        templateUrl: 'templates/tab-settings.html',
        controller: 'SettingsCtrl'
      }
    }
  });

  // Fallback
  $urlRouterProvider.otherwise( '/tab/timers' );
});

