angular.module( 'tallytime.services', ['tallytime.config'] )

/**
 * From https://gist.github.com/jgoux/10738978
 *
 * Creates initial database setup and provides ability to query the database
 * synchronously.
 */
.factory( 'DatabaseService', function( $q, DB_CONFIG ) {
  var self = this;
  self.database = null;

  self.init = function() {
    try {
      self.database = window.openDatabase(
        DB_CONFIG.dbName,
        DB_CONFIG.dbVersion,
        DB_CONFIG.dbDisplay,
        DB_CONFIG.dbSize
      );
    }
    catch( error ) {
      // Application is funked if this happens.
      console.error( "Could not create database." );
      console.dir( error );
    }

    angular.forEach( DB_CONFIG.tables, function( table ) {
      var columns = [];

      angular.forEach( table.columns, function( column ) {
        columns.push( column.name + ' ' + column.type );
      });

      self.query(
        'CREATE TABLE IF NOT EXISTS ' + table.name +
        ' (' + columns.join(',') + ')'
      );
    });

    // Drop all the views, then recreate them. This ensures all the views
    // are up to date.
    angular.forEach( DB_CONFIG.views, function( view ) {
      self.query( 'DROP VIEW IF EXISTS ' + view.name );
    });

    angular.forEach( DB_CONFIG.views, function( view ) {
      self.query(
        'CREATE VIEW ' + view.name + ' AS ' +
        view.sql
      );
    });
  }

  /**
   * Runs a promise query using self.database.
   *
   * @param query The SQL to execute.
   * @param bindings The optional parameters to pass as part of the query.
   */
  self.query = function( query, bindings ) {
    bindings = (typeof bindings !== 'undefined') ? bindings : [];
    var deferred = $q.defer();

    self.database.transaction( function( tx ) {
      tx.executeSql( query + ';', bindings,
        function( tx, result ) {
          deferred.resolve( result );
        },
        function( tx, error ) {
          console.log( "ERR: " + query );
          console.dir( error );
          deferred.reject( error );
        }
      );
    });

    return deferred.promise;
  };

  /**
   * Returns all database rows from a query.
   */
  self.fetchAll = function( result ) {
    var output = [];

    for( var i = 0; i < result.rows.length; i++ ) {
      output.push( result.rows.item( i ) );
    }
    
    return output;
  };

  /**
   * Returns the first database row from a query.
   */
  self.fetch = function(result) {
    return result.rows.item( 0 );
  };

  return self;
})

/**
 * Provides the ability to insert and update labels.
 */
.service( 'LabelService', function( DatabaseService ) {
  return {
    /**
     * Returns a list of all (non-deleted) labels from the database.
     */
    load: function() {
      return DatabaseService.query(
        "SELECT uuid, title, colour FROM label WHERE deleted IS NULL"
      ).then( function( result ) {
        return DatabaseService.fetchAll( result );
      });
    },

    /**
     * Inserts a new label record into the database.
     */
    insert: function( label ) {
      DatabaseService.query(
        "INSERT INTO label (title, colour, uuid) VALUES (?, ?, ?)",
        [label.title, label.bg, label.uuid]
      );
    },

    /**
     * Updates the title and colour for a particular label.
     */
    update: function( label ) {
      DatabaseService.query(
        "UPDATE label SET title = ?, colour = ? WHERE uuid = ?",
        [label.title, label.bg, label.uuid]
      );
    },

    /**
     * Deletes all labels by setting the deleted column to the current date.
     */
    purge: function() {
      DatabaseService.query(
        "UPDATE label SET DELETED = (strftime('%Y-%m-%d %H:%M:%f', 'NOW'))"
      );
    }
  }
})

/**
 * Provides core functionality: record the button press timestamp.
 */
.service( 'EventService', function( DatabaseService ) {
  return {
    stop: function() {
      // This breaks referential integrity; the stop button has no label fk.
      DatabaseService.query( "INSERT INTO event (label_pk) VALUES (-1)" );
    },

    insert: function( uuid ) {
      DatabaseService.query(
        "INSERT INTO event (label_pk) SELECT l.pk FROM label l WHERE (l.uuid=?) AND (l.deleted IS NULL) LIMIT 1",
        [uuid]
      );
    },

    insertAt: function( uuid, date ) {
      if( uuid == -1 ) {
        DatabaseService.query(
          "INSERT INTO event (label_pk, tallied) VALUES (-1, ?)",
          [date]
        );
      }
      else {
        DatabaseService.query(
          "INSERT INTO event (label_pk, tallied) SELECT l.pk, ? FROM label l WHERE (l.uuid=?) AND (l.deleted IS NULL) LIMIT 1",
          [date, uuid]
        );
      }
    }
  }
})

/**
 * Provides functionality for selecting report types and running reports.
 */
.factory( 'ReportFactory', function(
  DatabaseService,
  LocalStorageService,
  ColourService ) {

  var report = {
    type       : LocalStorageService.get( 'report.type', 'Time' ),
    aggregate  : LocalStorageService.get( 'report.aggregate', 'Day' ),
    eventLabels: [],
    events     : [],
    aggregates : [
      { id: 'Day' },
      { id: 'Week' },
      { id: 'Month' },
      { id: 'Year' }
    ],
    types: [
      { id: 'Time' },
      { id: 'Tally' }
    ]
  };

  return {
    /**
     * Return a reference to the report that contains form input parameter
     * values and user settings.
     */
    reportParameters: function() {
      return report;
    },

    /**
     * Retrieve all the event labels, sorted by title.
     */
    queryEventLabels: function() {
      return DatabaseService.query(
        "SELECT l.pk AS pk, l.title AS title, l.colour AS colour FROM duration_total_" + report.aggregate.toLowerCase() + "_vw d, label l WHERE d.label_pk = l.pk GROUP BY l.pk, l.title ORDER BY l.title"
      )
    },

    /**
     * Retrieve all the events with durations in seconds, sorted by title.
     */
    queryTimes: function() {
      return DatabaseService.query(
        "SELECT l.pk AS pk, dt.event AS event, dt.duration AS duration FROM duration_total_" + report.aggregate.toLowerCase() + "_vw dt, label l WHERE dt.label_pk = l.pk ORDER BY dt.event, l.title"
      )
    },

    /**
     * Retrieve all the events, tallied and sorted by title.
     */
    queryTallies: function() {
      return DatabaseService.query(
        "SELECT l.pk AS pk, et.event AS event, et.tally AS tally FROM event_tally_vw et, label l WHERE et.label_pk = l.pk ORDER BY et.event, l.title"
      )
    }
  }
})

/**
 * Provides functionality to produce a unique identifier.
 */
.service( 'IdentifierService', function() {
  return {
    /**
     * Generate a unique identifer for the button.
     * See also: http://stackoverflow.com/a/8809472/59087
     */
    uuid: function() {
      var d = new Date().getTime();
      var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace( /[xy]/g,
        function( c ) {
          var r = (d + Math.random() * 16) % 16 | 0;

          d = Math.floor( d / 16 );

          return (c == 'x' ? r : (r & 0x3 | 0x8)).toString( 16 );
        });

      return uuid;
    }
  }
})

/**
 * Provides functionality to convert seconds to H:M:S.
 */
.factory( 'TimerService', function() {
  // Tracks when the timer was started (ignores page loads).
  var started = null;

  // Tracks the active button uuid.
  var uuid = null;

  return {
    /**
     * Converts an elapsed number of seconds into hours, minutes, and seconds.
     */
    toHMS: function( seconds ) {
      var h = Math.floor( seconds / 3600 );

      seconds %= 3600;

      var m = Math.floor( seconds / 60 );
      var s = seconds % 60;

      return {
        h: (h < 10 ? "0" + h : h),
        m: (m < 10 ? "0" + m : m),
        s: (s < 10 ? "0" + s : s)
      };
    }
  }
})

/**
 * Provides ways to read from and write to local storage (limited capacity).
 */
.factory( 'LocalStorageService', [
  '$window',
  function( $window ) {
    return {
      set: function( key, value ) {
        $window.localStorage[ key ] = value;
      },
      get: function( key, defaultValue ) {
        return $window.localStorage[ key ] || defaultValue;
      },
      setObject: function( key, value ) {
        $window.localStorage[ key ] = JSON.stringify( value );
      },
      getObject: function( key ) {
        return JSON.parse( $window.localStorage[ key ] || '{}' );
      },
      removeObject: function( key ) {
        return $window.localStorage.removeItem( key );
      }
    }
  }
])

/**
 * Provides calculations to help select a harmonious foreground colour.
 */
.service( 'ColourService', function() {
  return {
    /**
     * Returns a true to indicate that a light colour will contrast best with
     * the given colour.
     *
     * @param hex The hexadecimal notation for an RGB colour.
     */
    contrast: function( hex ) {
      return this.luma( hex ) >= 155;
    },

    /**
     * Returns a luma value based on the given hex colour.
     *
     * See: https://en.wikipedia.org/wiki/Rec._709#Luma_coefficients
     */
    luma: function( hex ) {
      var rgb = this.hexToRGB( hex );
      return 0.2126729 * rgb[0] + 0.7151522 * rgb[1] + 0.0721750 * rgb[2];
    },

    /**
     * Converts a hex RGB string (with or without #) into an array.
     *
     * @param hex The hexadecimal colour value to convert to array of R, G,
     * and B values (0 to 255).
     */
    hexToRGB: function( hex ) {
      // Remove everything except numbers and A-F (case insensitive).
      hex = hex.replace( /[^0-9A-F]/gi, '' );

      // Parse the integer value for the hex code.
      var i = parseInt( hex, 16 );

      // Bit shift and twiddle to extract the R, G, and B values.
      var r = (i >> 16) & 255;
      var g = (i >> 8) & 255;
      var b = i & 255;

      return [r, g, b];
    }
  }
});

