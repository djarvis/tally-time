angular.module( 'tallytime.config', [] )

.constant( 'DB_CONFIG', {
  dbName: 'tallytime',
  dbVersion: '1.0',
  dbDisplay: 'Tally Time',
  dbSize: 5 * 1000 ^ 2,
  tables: [
    {
      // 31 bytes per row
      name: 'event',
      columns: [
        {
          // 4 bytes
          name: 'pk',
          type: "INTEGER PRIMARY KEY"
        },
        {
          // 4 bytes
          name: 'label_pk',
          type: "INTEGER"
        },
        {
          // 23 bytes, UTC
          name: 'tallied',
          type: "DATETIME DEFAULT (STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW'))"
        }
      ]
    },
    {
      name: 'label',
      columns: [
        {
          name: 'pk',
          type: "INTEGER PRIMARY KEY"
        },
        {
          name: 'uuid',
          type: "TEXT"
        },
        {
          name: 'title',
          type: "TEXT"
        },
        {
          name: 'colour',
          type: "TEXT"
        },
        {
          name: 'deleted',
          type: "DATETIME DEFAULT NULL"
        }
      ]
    }
  ],
  views: [
    {
      // Lists event times ordered by timestamp. Used by the duration_vw
      // to calculate the duration between times. Cannot 'ORDER BY tallied'
      // on devices as it results in an "ORDER BY term out of range" error.
      // Must 'ORDER BY 1', instead. Error not present when testing against
      // Chromium.
      name: 'event_vw',
      sql: "SELECT label_pk, event_pk, tallied FROM (SELECT l.pk AS label_pk, e.pk AS event_pk, e.tallied FROM event e, label l WHERE e.label_pk = l.pk AND l.deleted IS NULL UNION ALL SELECT -1 AS label_pk, e.pk AS event_pk, e.tallied FROM event e WHERE e.label_pk = -1) ORDER BY 1",
    },
    {
      // Lists event times and relative durations between times.
      name: 'duration_vw',
      sql: "SELECT eo.label_pk AS label_pk, eo.event_pk AS event_pk, eo.tallied AS tallied, strftime( '%s', coalesce( (SELECT ei.tallied FROM event_vw ei WHERE ei.event_pk > eo.event_pk LIMIT 1), eo.tallied )) - strftime( '%s', eo.tallied ) AS duration FROM event_vw eo"
    },
    {
      // Calculates the total sum for consecutive durations on different days.
      name: 'duration_total_day_vw',
      sql: 'SELECT label_pk, strftime("%Y-%m-%d", tallied) AS event, sum(duration) AS duration FROM duration_vw WHERE label_pk <> -1 GROUP BY label_pk, event ORDER BY event'
    },
    {
      // Calculates the total sum for consecutive durations on different weeks.
      name: 'duration_total_week_vw',
      sql: 'SELECT label_pk, strftime( "%Y-%m-%d", (max( date( tallied, "weekday 0", "-7 day" )))) AS event, strftime("%W", tallied) AS week, sum(duration) AS duration FROM duration_vw WHERE label_pk <> -1 GROUP BY label_pk, week ORDER BY week'
    },
    {
      // Calculates the total sum for consecutive durations during a month.
      name: 'duration_total_month_vw',
      sql: 'SELECT label_pk, strftime("%Y-%m", tallied) AS event, sum(duration) AS duration FROM duration_vw WHERE label_pk <> -1 GROUP BY label_pk, event ORDER BY event'
    },
    {
      // Calculates the total sum for consecutive durations during a year.
      name: 'duration_total_year_vw',
      sql: 'SELECT label_pk, strftime("%Y", tallied) AS event, sum(duration) AS duration FROM duration_vw WHERE label_pk <> -1 GROUP BY label_pk, event ORDER BY event'
    },
    {
      // Calculates the total number of individual button presses per day.
      name: 'event_tally_vw',
      sql: 'SELECT label_pk, strftime("%Y-%m-%d", tallied) AS event, count(label_pk) AS tally FROM duration_vw WHERE label_pk <> -1 GROUP BY event, label_pk ORDER BY event'
    }
  ]
});

