angular.module( 'tallytime.controllers', ['tallytime.services'] )

.controller( 'LabelsCtrl', function(
  $scope,
  $ionicScrollDelegate,
  $state,
  $timeout,
  LocalStorageService,
  LabelService,
  ColourService,
  EventService,
  IdentifierService,
  TimerService ) {

  // Scope for Tally Time options (user settings, counter, etc.).
  $scope.opt = {};

  /**
   * Answers whether the stop button has been set.
   */
  $scope.hasStopButton = function() {
    return LocalStorageService.get( 'stopButton', 'false' ) === 'true';
  };

  /**
   * Answers whether the delete button has been set.
   */
  $scope.hasDeleteButton = function() {
    return LocalStorageService.get( 'deleteButton', 'false' ) === 'true';
  };

  /**
   * Sets $scope.labels to the list of all non-deleted available labels.
   */
  $scope.loadLabels = function() {
    LabelService.load().then( function( labels ) {
      if( typeof $scope.labels === "undefined" ) {
        $scope.labels = new Array();
      }

      for( var i = 0; i < labels.length; i++ ) {
        var row = labels[i];
        var label = {
          uuid: row.uuid,
          title: row.title,
          bg: row.colour,
          fg: ColourService.contrast( row.colour )
        };

        $scope.labels.push( label );
      }

      // Ensure one label always exists.
      if( $scope.labels.length == 0 ) {
        $scope.createLabel();
      }
    });
  };

  /**
   * Called when the user opts to add a new label. This is also called to
   * ensure there is at least one label. This will add the label into the
   * database.
   */
  $scope.createLabel = function() {
    var uuid = IdentifierService.uuid();
    var label = {
      uuid: uuid,
      title: 'Button Label',
      bg: '#000000',
      fg: ColourService.contrast( '#000000' )
    };

    LabelService.insert( label );

    $scope.labels.push( label );

    // This will save the labels, including the newly added timer. This
    // also generates a new, random, set of colours for all the labels.
    this.updateColours();

    // Scroll to the bottom when many labels exist. This helps with editing.
    if( $scope.labels.length > 5 ) {
      $ionicScrollDelegate.scrollBottom();
    }
  };

  /**
   * Persists a revised label. The label (uuid) must exist in the database
   * prior to calling.
   */
  $scope.updateLabel = function( index ) {
    LabelService.update( $scope.labels[index] );
  };

  /**
   * Provides a new set of colours for the labels and persists the list.
   */
  $scope.updateColours = function() {
    // Number of colours to generate.
    var len = $scope.labels.length;

    // Generate a unique set of colours for the labels.
    var palette = paletteGenerator.generate(
      len,
      function( color ) {
        var hcl = color.hcl();

        // See: http://tools.medialab.sciences-po.fr/iwanthue/palettes.php
        // Bright, colourful, semi-pastels
        return hcl[0] >= 0.0  && hcl[0] <= 360  &&
               hcl[1] >= 0.65 && hcl[1] <= 1.55 &&
               hcl[2] >= 1.0  && hcl[2] <= 1.5;
      },
      false,
      100
    );

    palette = paletteGenerator.diffSort( palette ).map( function( color ) {
      return color.hex()
    });

    for( var i = 0; i < len; i++ ) {
      var label = $scope.labels[i];
      label.bg = palette[i];
      label.fg = ColourService.contrast( label.bg );

      LabelService.update( label );
    }
  };

  /**
   * Called when the user requests to delete all the labels. This is
   * predominately meant for testing. The button is enabled through settings.
   */
  $scope.deleteLabels = function() {
    LabelService.purge();
    $state.go( $state.current, {}, {reload: true} );
  };

  /**
   * Called when the user presses the stop button. Also called when the user
   * presses a tally button (to reset the timer).
   */
  $scope.stopTimer = function() {
    TimerService.started = null;

    if( $scope.timer !== undefined && $scope.timer.id !== null ) {
      $timeout.cancel( $scope.timer.id );
      $scope.timer.id = null;
      $scope.timer.uuid = null;
    }
  };

  /**
   * Called when the user clicks a tally button.
   */
  $scope.startTimer = function() {
    TimerService.started = $scope.timer.started;
    TimerService.uuid = $scope.timer.uuid;

    (function tick() {
      $scope.timer.id = $timeout( tick, 1000 );

      // Elapsed time in seconds.
      var elapsed = Math.round( (new Date() - $scope.timer.started) / 1000 );
      var hms = TimerService.toHMS( elapsed );

      $scope.timer.elapsed = hms.h + ":" + hms.m + ":" + hms.s;
    })();
  };

  /**
   * Called after the user presses a timer button. This registers the event
   * in the database.
   */
  $scope.tally = function( uuid ) {
    this.stopTimer();

    if( uuid === 'stop' ) {
      EventService.stop();
    }
    else {
      EventService.insert( uuid );

      $scope.timer = {
        id: null,
        uuid: uuid,
        started: new Date()
      };

      this.startTimer();
    }
  };

  /**
   * Called to initialize the scope labels and settings variables.
   */
  $scope.init = function() {
    this.loadLabels();

    $scope.opt.stopButton =
      LocalStorageService.get( "stopButton", "false" );
    $scope.opt.deleteButton =
      LocalStorageService.get( "deleteButton", "false" );
  };

  // Restart the timer service upon navigation page changes.
  if( TimerService.started !== undefined && TimerService.started !== null ) {
    $scope.timer = {
      uuid: TimerService.uuid,
      started: TimerService.started
    };

    $scope.startTimer();
  }

  $scope.init();
})

.controller( 'EditsCtrl', function( $scope, LabelService, EventService ) {
  // Default to adding a timer at the current date and time.
  var now = new Date();

  now.setSeconds( 0 );
  now.setMilliseconds( 0 );

  $scope.edits = {
    date: now,
    time: now,
    label: null,
    labels: []
  };

  // TODO: Refactor this into LoadService?
  LabelService.load().then( function( labels ) {
    for( var i = 0; i < labels.length; i++ ) {
      var row = labels[i];
      var label = {
        uuid: row.uuid,
        title: row.title
      };

      $scope.edits.labels.push( label );
    }

    // Append the ability to add an arbitrary stop.
    var label = {
      uuid: -1,
      title: 'Stop'
    };

    $scope.edits.labels.push( label );

    // Prevent an empty element in the drop-down by selecting the first label.
    $scope.edits.label = $scope.edits.labels[0];
  });

  /**
   * Inserts an event with a user-provided timestamp.
   */
  $scope.insertTally = function() {
    var uuid = $scope.edits.label.uuid;
    var date = new Date( $scope.edits.date );
    date.setHours( $scope.edits.time.getHours() );
    date.setMinutes( $scope.edits.time.getMinutes() );

    // Prototype for format({...}) is defined in app.js.
    var tallied = "{year}-{month}-{day} {hour}:{minute}:00.000".format({
      year: date.getUTCFullYear(),
      month: ("0" + (date.getUTCMonth() + 1)).substr( -2 ),
      day: ("0" + date.getUTCDate()).substr( -2 ),
      hour: ("0" + date.getUTCHours()).substr( -2 ),
      minute: ("0" + date.getUTCMinutes()).substr( -2 )
    });

    EventService.insertAt( uuid, tallied );
  };
})

.controller( 'ReportsCtrl', function(
  $scope,
  $state,
  ReportFactory,
  LocalStorageService ) {

  $scope.factory = ReportFactory;
  $scope.factory.report = ReportFactory.reportParameters();

  $scope.run = function() {
    switch( $scope.factory.report.type ) {
      case 'Time':
        $state.go( 'tab.reports-time' );
        break;

      case 'Tally':
        $state.go( 'tab.reports-tally' );
        break;
    }
  };

  $scope.updateReportType = function( item ) {
    LocalStorageService.set( 'report.type', item.id );
  };

  $scope.updateReportAggregate = function( item ) {
    LocalStorageService.set( 'report.aggregate', item.id );
  };
})

.controller( 'ReportsTimeCtrl', function(
  $scope,
  eventLabels,
  times,
  DatabaseService,
  ColourService,
  TimerService ) {

  $scope.chart = {
    labels : [],
    colours: [],
    data   : [],
    series : []
  };

  // Clear the previous report run totals from memory.
  $scope.report = {
    eventLabels: {
      labels: [],
      indices: {}
    },
    events: [],
    totals: [],
    times: []
  };

  var labels = DatabaseService.fetchAll( eventLabels );
  var events = DatabaseService.fetchAll( times );

  // Extract the column names.
  for( var i = 0; i < labels.length; i++ ) {
    var label = {
      pk: labels[i].pk,
      title: labels[i].title,
      bg: labels[i].colour,
      fg: ColourService.contrast( labels[i].colour )
    };

    $scope.chart.labels.push( label.title );
    $scope.chart.series.push( label.title );
    $scope.chart.colours.push( label.bg );

    $scope.report.eventLabels.indices[ label.pk ] = i;
    $scope.report.eventLabels.labels.push( label );

    $scope.report.totals[i] = 0;
  }

  var eventDate = null;
  var row = {};

  // Create an array of arrays to associate the tallies (new column) with
  // durations and pks (rows per column).
  var indices = $scope.report.eventLabels.indices;

  for( var i = 0; i < events.length; i++ ) {
    var e = events[i];
    var hms = TimerService.toHMS( e.duration );
    var duration = {};

    duration.pk = e.pk;
    duration.h  = hms.h;
    duration.m  = hms.m;
    duration.s  = hms.s;

    // Determines the column to display the duration.
    duration.index = indices[e.pk];

    // Used to help calculate the total sum.
    $scope.report.totals[ indices[e.pk] ] += e.duration;

    // Convert the total sums to H:M:S format.
    $scope.report.times[ indices[e.pk] ] = TimerService.toHMS(
      $scope.report.totals[ indices[e.pk] ] );

    if( e.event !== eventDate ) {
      eventDate = e.event;

      // Track the new list of items associated with the event date.
      row = {
        date: eventDate,
        durations: []
      };

      $scope.report.events.push( row );
    }

    // Expose the newfound duration to the report events.
    row.durations[ indices[ e.pk ] ] = duration;
  }

  for( var i = 0; i < labels.length; i++ ) {
    $scope.chart.data.push( $scope.report.totals[i] );
  }
})

.controller( 'ReportsTallyCtrl', function(
  $scope,
  eventLabels,
  tallies,
  DatabaseService,
  ColourService ) {

  $scope.chart = {
    labels: [],
    datasets: [],
    series: [],
    colours: []
  };

  $scope.report = {};
  $scope.report.eventLabels = {
    labels: [],
    indices: {}
  };

  var labels = DatabaseService.fetchAll( eventLabels );
  var events = DatabaseService.fetchAll( tallies );

  // Extract the column names.
  for( var i = 0; i < labels.length; i++ ) {
    var label = {
      pk: labels[i].pk,
      title: labels[i].title,
      bg: labels[i].colour,
      fg: ColourService.contrast( labels[i].colour )
    };

    $scope.chart.series.push( label.title );
    $scope.chart.colours.push( label.bg );

    $scope.report.eventLabels.indices[ label.pk ] = i;
    $scope.report.eventLabels.labels.push( label );
  }

  // Create an array of arrays to associate the tallies (new column) with
  // durations and pks (rows per column).
  var indices = $scope.report.eventLabels.indices;

  var eventDate = null;
  var row = {};

  // Clear the previous report run from memory.
  $scope.report.tallies = [];
  $scope.report.totals = [];

  for( var i = 0; i < events.length; i++ ) {
    $scope.report.totals[i] = 0;

    var e = events[i];
    var tally = {};

    tally.pk    = e.pk;
    tally.tally = e.tally;

    // Determines the column to display the tally.
    tally.index = indices[e.pk];

    // Calculate the total sums.
    $scope.report.totals[ indices[e.pk] ] += e.tally;

    if( e.event !== eventDate ) {
      eventDate = e.event;

      // Track the new list of items associated with the event date.
      row = {
        date: eventDate,
        tallies: []
      };

      $scope.report.tallies.push( row );
    }

    // Expose the newfound tally to the report events.
    row.tallies[ indices[e.pk] ] = tally;
  }

  var orderedEvents = $scope.report.tallies;

  // Push dates along the X-axis and tallies up the Y-axis.
  for( var i = 0; i < orderedEvents.length; i++ ) {
    $scope.chart.labels.push( orderedEvents[i].date );

    var tallies = orderedEvents[i].tallies;

    for( var t = 0; t < labels.length; t++ ) {
      if( typeof $scope.chart.datasets[t] === "undefined" ) {
        $scope.chart.datasets[t] = [];
      }

      $scope.chart.datasets[t].push(
        (typeof tallies[t] === "undefined") ? 0 : tallies[t].tally
      );
    }
  }
})

.controller( 'SettingsCtrl', function( $scope, LocalStorageService ) {
  // Scope for Tally Time options (user settings, counter, etc.).
  $scope.opt = {};

  $scope.opt.stopButton =
    LocalStorageService.get( 'stopButton', 'false' ) === 'true';
  $scope.opt.deleteButton =
    LocalStorageService.get( 'deleteButton', 'false' ) === 'true';

  $scope.updateStopButton = function() {
    LocalStorageService.set( 'stopButton', $scope.opt.stopButton );
  };

  $scope.updateDeleteButton = function() {
    LocalStorageService.set( 'deleteButton', $scope.opt.deleteButton );
  };
})

